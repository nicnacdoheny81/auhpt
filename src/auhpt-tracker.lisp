;;;; The auhpt-tracker program is one of the modules used by the Advanced UNIX Hybrid Package-Management Toolkit. It is used to manage the tracking database which keeps tracks of:
;;;; - PACKAGE-NAME: The name of the package (example: editors/vim or editors/vim-0.4.3).
;;;; - BINARY-NAME: The name of the binary the program is installed as.
;;;; - VERSION: The version of the installed program.
;;;; - PROVIDER: The organization or peoples who provided the software (not necessarily the author).
;;;; - INSTALLATION-STATE: The state of installation for the program.
;;;; - INSTALLATION-METHOD: How the program was installed (either as a binary or as a source build).
;;;; - DEPENDENCY-STATUS: Whether the program was installed as a dependency or not.
;;;; - ASSOCIATED-FILES: What files (outside of HOME) are from the program.
;;;; - USE-FLAGS: The USE-FLAGS the program was built with.
;;;; - ATTACHED-DEPENDENCIES: The dependencies installed with the program.
;;;; This meta data is stored as a plist in the AUHPT_DB file. The auhpt-tracker module takes the following arguments:
